package com.evvemi.braintree;

import java.math.BigDecimal;
import java.util.*;

import com.braintreegateway.*;
import com.braintreegateway.Transaction.Status;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CheckoutController {

    private BraintreeGateway gateway = BraintreeApplication.gateway;

     private Status[] TRANSACTION_SUCCESS_STATUSES = new Status[] {
        Transaction.Status.AUTHORIZED,
        Transaction.Status.AUTHORIZING,
        Transaction.Status.SETTLED,
        Transaction.Status.SETTLEMENT_CONFIRMED,
        Transaction.Status.SETTLEMENT_PENDING,
        Transaction.Status.SETTLING,
        Transaction.Status.SUBMITTED_FOR_SETTLEMENT
     };
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(Model model) {
        return "redirect:checkouts";
    }
    @RequestMapping(value = "/checkouts", method = RequestMethod.GET)
    public String checkout(Model model) {
        String clientToken = gateway.clientToken().generate();
        model.addAttribute("clientToken", clientToken);

        return "checkouts/new";
    }
    @RequestMapping(value = "payment/{email}", method = RequestMethod.GET)
    public String root(@PathVariable String email, Model model) {
        Map<String,String> evvemi_users = loadUsers();
        System.out.println(email);
        String braintree_user_id= evvemi_users.get(email);
        if(braintree_user_id==null)
        {
            CustomerServices customerServices = new CustomerServices();
            braintree_user_id =customerServices.addCustomer("Steve","Smith");
        }
        System.out.println(braintree_user_id);
        return "redirect:checkouts/"+braintree_user_id;
    }

    private Map<String,String> loadUsers() {
        Map<String,String> evvemi_users = new HashMap<>();
        evvemi_users.put("first@email","839220919");
        evvemi_users.put("second@email","583543366");
        evvemi_users.put("third@email","889597016");
        evvemi_users.put("fourth@email","866341516");
        return evvemi_users;
    }

    @RequestMapping(value = "payment/checkouts/{customer_id}", method = RequestMethod.GET)
    public String checkout(@PathVariable String customer_id, Model model) {
        System.out.println(customer_id);
        ClientTokenRequest  clientTokenRequest = new ClientTokenRequest();
        String clientToken = gateway.clientToken().generate(clientTokenRequest.customerId(customer_id));

        model.addAttribute("clientToken", clientToken);
        return "checkouts/new";
    }

    @RequestMapping(value = "/checkouts", method = RequestMethod.POST)
    public String postForm(@RequestParam("amount") String amount, @RequestParam("payment_method_nonce") String nonce, Model model, final RedirectAttributes redirectAttributes) {
        BigDecimal decimalAmount;
        try {
            decimalAmount = new BigDecimal(amount);
        } catch (NumberFormatException e) {
            redirectAttributes.addFlashAttribute("errorDetails", "Error: 81503: Amount is an invalid format.");
            return "redirect:checkouts";
        }
        /*CustomerServices CF = new CustomerServices();
        String token = CF.addPaymentMethod("839220919",nonce);
        if(token!=null)
            System.out.println("Token is "+token);
        else
            System.out.println("Payment method not entered ");

        return "redirect:/getCards";*/
        TransactionRequest request = new TransactionRequest()
            .amount(decimalAmount)
            .paymentMethodNonce(nonce)
            .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(request);

        if (result.isSuccess()) {
            Transaction transaction = result.getTarget();
            return "redirect:checkouts/" + transaction.getId();
        } else if (result.getTransaction() != null) {
            Transaction transaction = result.getTransaction();
            return "redirect:checkouts/" + transaction.getId();
        } else {
            String errorString = "";
            for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
               errorString += "Error: " + error.getCode() + ": " + error.getMessage() + "\n";
            }
            redirectAttributes.addFlashAttribute("errorDetails", errorString);
            return "redirect:checkouts";
        }
    }

    @RequestMapping(value = "/checkouts/{transactionId}")
    public String getTransaction(@PathVariable String transactionId, Model model) {
        Transaction transaction;
        CreditCard creditCard;
        Customer customer;

        try {
            transaction = gateway.transaction().find(transactionId);
            creditCard = transaction.getCreditCard();
            customer = transaction.getCustomer();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return "redirect:/checkouts";
        }

        model.addAttribute("isSuccess", Arrays.asList(TRANSACTION_SUCCESS_STATUSES).contains(transaction.getStatus()));
        model.addAttribute("transaction", transaction);
        model.addAttribute("creditCard", creditCard);
        model.addAttribute("customer", customer);

        return "checkouts/show";
    }




    @RequestMapping(value="/getCards",method = RequestMethod.GET)
    public @ResponseBody Customer getCustomer()
    {
        CustomerServices CS = new CustomerServices();
        //CF.addCustomer();
        return CS.getCustomer("839220919");

    }
}
