package com.evvemi.braintree;

import com.braintreegateway.*;

public class CustomerServices {

    private BraintreeGateway gateway = BraintreeApplication.gateway;

    public String addCustomer(String firstName,String lastName)
    {
        String customer_id = null;
        CustomerRequest request = new CustomerRequest()
                .firstName(firstName)
                .lastName(lastName);
        Result<Customer> result = gateway.customer().create(request);
        if(result.isSuccess())
        {
            Customer customer = result.getTarget();
            customer_id = customer.getId();
            //System.out.println(customer.getId());
            // System.out.println("Generate token is "+customer.getPaymentMethods().get(0).getToken());

        }
        return customer_id;
    }

    public Customer getCustomer(String customer_id)
    {
        Customer customer = null;
        try {
             customer = gateway.customer().find("839220919");

        }catch (Exception e)
        {
            System.out.println("Customer not found");
        }
        return customer;
    }

    public String addPaymentMethod(String customer_id,String nonce)
    {
        /*PaymentMethodRequest request = new PaymentMethodRequest()
                .customerId(customer_id)
                .paymentMethodNonce(nonce);*/
        //Result<? extends PaymentMethod> result = gateway.paymentMethod().create(request);
        //String token = result.getTarget().getToken();

        PaymentMethodRequest request = new PaymentMethodRequest()
                .options()
                    .makeDefault(true)
                .done();
        Result<? extends PaymentMethod> result = gateway.paymentMethod().update("d6nv48",request);

        String token = gateway.customer().find("839220919").getDefaultPaymentMethod().getToken();
        //String token = gateway.customer().find("839220919").getPaymentMethods().get(0).getToken();
        return token;
    }

}
